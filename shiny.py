import importlib
import sys

_passed = sys.argv[1:]
arguments = {}
for index in range(0, len(_passed), 2):
    arguments[_passed[index][2:]] = _passed[index + 1]


module = importlib.import_module(
    arguments.get("module"),
    package=arguments.get("package", None)
)

module.run()