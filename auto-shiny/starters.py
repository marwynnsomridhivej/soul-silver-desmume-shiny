import datetime
import pyautogui
import pydirectinput

from ._enter_game import check_for_load, go_into_game, reset
from .config import config
from .core import get_starter_match


def _check_for_match() -> bool:
    go_into_game()
    check_for_load()
    pydirectinput.press(config["a"], presses=2, interval=1)
    for _ in range(3):
        pydirectinput.press(config["left"])
        if get_starter_match():
            break
        print("NO MATCH")
    else:
        reset()
        return False
    print("FOUND MATCH")
    return True


def run() -> None:
    start = int(datetime.datetime.now().timestamp())
    found = False
    resets = 0
    while not found:
        found = _check_for_match()
        if not found:
            resets += 1
            print(f"[RESET {resets}]: Shiny not found")
    finish = int(datetime.datetime.now().timestamp())
    print(
        f"""Shiny found after {resets} reset{'s' if resets != 1 else ''}
Time elapsed: {datetime.timedelta(seconds=finish - start)}
""")
