import json


with open("./config.json", "r") as file:
    config: dict = json.loads(file.read())
