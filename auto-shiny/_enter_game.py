import os
import time

import pyautogui
import pydirectinput

from ._region import get_region
from .config import config
from .utils import hotkey

GAME_FREAK = os.path.abspath("./img/gamefreakpresents.png")
SAVE_SELECT = os.path.abspath("./img/saveselect.png")
OPTIONS = os.path.abspath("./img/options.png")


def go_into_game() -> None:
    while not pyautogui.locateOnScreen(GAME_FREAK, region=get_region()):
        time.sleep(0.2)
    while not pyautogui.locateOnScreen(SAVE_SELECT, region=get_region()):
        pydirectinput.press(config["a"])
        time.sleep(0.5)
    pydirectinput.press(config["a"])
    return


def check_for_load():
    while not pyautogui.locateOnScreen(OPTIONS, region=get_region()):
        pass
    return


def reset() -> None:
    reset = str(config["reset"]).split(" ")
    hotkey(*reset)
    return
