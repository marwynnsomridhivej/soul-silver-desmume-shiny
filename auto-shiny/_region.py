from typing import Tuple
from win32gui import FindWindow, GetWindowRect
import pywintypes
from .config import config


def get_region() -> Tuple[int, int, int, int]:
    ret = None
    while ret is None:
        window_handle = FindWindow(None, config["window_name"])
        try:
            ret = GetWindowRect(window_handle)
        except pywintypes.error:
            continue
        break
    return ret
