import os
from typing import List

import pyautogui

from ._region import get_region
from .config import config

SPRITE_PATHS = {}
NORMAL = frozenset([filename for filename in os.listdir(config["sprites_location"]) if filename.endswith(".png")])
FEMALE_VARIANT = frozenset([
    filename for filename in os.listdir(
        f"{config['sprites_location']}\\female"
    ) if filename.endswith(".png")
])

for filename in NORMAL:
    payload = [f"{config['sprites_location']}\\{filename}"]
    if filename in FEMALE_VARIANT:
        payload.append(f"{config['sprites_location']}\\female\\{filename}")
    SPRITE_PATHS[filename[:-4]] = payload


def get_match(matches: List[int], confidence: float = config["confidence"]) -> bool:
    for query in matches:
        for option in SPRITE_PATHS[str(query)]:
            if pyautogui.locateOnScreen(option, confidence=confidence, grayscale=True, region=get_region()):
                break
        else:
            continue
        break
    else:
        return False
    return True


def get_starter_match() -> bool:
    _base = "./img/starters/"
    for filename in os.listdir(_base):
        if pyautogui.locateOnScreen(os.path.abspath(_base + filename)):
            return True
    return False
