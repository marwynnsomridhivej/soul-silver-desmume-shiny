import pyautogui

_TEMPLATES = [
    "pyautogui.keyDown('{}')\n",
    "pyautogui.keyUp('{}')\n",
]


def hotkey(*keys):
    to_exec = ""
    for template in _TEMPLATES:
        for key in keys:
            to_exec += template.format(key)
    exec(to_exec)
